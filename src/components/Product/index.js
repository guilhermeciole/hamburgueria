import "./style.css"

function Product({ handleClick, item }) {
  return (
    <div className="product">
      <h1>{item.name}</h1>
      <p>Categoria: {item.category}</p>
      <p>Preço: R${item.price}</p>
      <button onClick={() => handleClick(item.id)}>Adicionar ao carrinho</button>
    </div>
  );
}

export default Product;
