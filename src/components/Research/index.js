import { useState } from "react";
import "./style.css"

function Research({ showProducts }) {
  const [userInput, setUserInput] = useState("");

  return (
    <div>
      <input
        type="text"
        value={userInput}
        onChange={(e) => setUserInput(e.target.value)}
      />
      <button onClick={() => {showProducts(userInput)}}>Pesquisar</button>
    </div>
  );
}

export default Research;