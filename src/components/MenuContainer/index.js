import Product from "../Product";
import "./style.css"

function MenuContainer({ products, handleClick }) {
  return (
    <div className="menu">
      {products.map((item) => {
        return <Product item={item} handleClick={handleClick} />;
      })}
    </div>
  );
}

export default MenuContainer;
