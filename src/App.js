import "./App.css";
import { useState } from "react";
import MenuContainer from "./components/MenuContainer";
import Research from "./components/Research";
import { log } from "loglevel";

function App() {
  const [products, setProducts] = useState([
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ]);

  const [filteredProducts, setFilteredProducts] = useState([]);
  const [currentSale, setCurrentSale] = useState([]);

  function showProducts(valor) {
    products.filter((item) => {
      if (item.name === valor) {
        setFilteredProducts([
          <h1>{item.name}</h1>,
          <p>Categoria: {item.category}</p>,
          <p>Preço: R${item.price}</p>,
          <button onClick={() => handleClick(item.id)}>
            Adicionar ao carrinho
          </button>,
        ]);
      }
    });
  }

  function handleClick(productId) {
    if (!currentSale.map((item) => item.id).includes(productId)) {
      const AddCar = products.find((item) => {
        return item.id === productId;
      });
      setCurrentSale([...currentSale, AddCar]);
    }
  }

  const itensCart = currentSale.map((item) => {
    return [
      <div className="itensCart">
        <h3>{item.name}</h3>
        <p>Categoria: {item.category}</p>
        <p>Preço: R${item.price}</p>
      </div>,
    ];
  });

  const total = currentSale.reduce((acc, item) => {
    return acc + item.price;
  }, 0);

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="titulo">Cardápio</h1>
        <Research showProducts={showProducts} />
        <div className="research">{filteredProducts}</div>
        <div className="price">
          <p>Subtotal: R${total}</p>
        </div>
        <MenuContainer handleClick={handleClick} products={products} />
        <div className="cart">
          <h1>Carrinho:</h1>
          {itensCart}
        </div>
      </header>
    </div>
  );
}

export default App;
